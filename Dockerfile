FROM python:3.10-buster
ENV PYTHONUNBUFFERED=1

RUN apt-get update && apt-get upgrade -y && apt-get install -y postgresql-client

COPY requirements.txt /temp/requirements.txt

ADD . /code/
WORKDIR /code
EXPOSE 8000

RUN pip install -r /temp/requirements.txt

RUN adduser  --disabled-password user
USER user

