from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from .const import TIME_BEFORE_DELETE
from celery_app import app

# Create your models here.


class Character(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=20, unique=True)
    level = models.PositiveSmallIntegerField(default=1)
    deletion_scheduled = models.DateTimeField(null=True, blank=True)
    deletion_task_id = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["id"]

    def schedule_delete(self):
        from .tasks import delete_character_after_delay

        if self.deletion_task_id:
            self.unschedule_delete()

        self.deletion_scheduled = timezone.now()
        # Schedule new deletion task
        task = delete_character_after_delay.apply_async(
            args=[self.id], countdown=TIME_BEFORE_DELETE * 60 + 5
        )
        # Store the task id
        self.deletion_task_id = task.id
        self.save()

    def unschedule_delete(self):
        # Revoke the task and clear the task id
        if self.deletion_task_id:
            app.control.revoke(task_id=self.deletion_task_id, terminate=True)
            self.deletion_task_id = None
            self.deletion_scheduled = None
            self.save()
