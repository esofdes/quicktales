import re
from datetime import timedelta

from django.contrib.auth.models import User
from rest_framework import serializers

from .const import TIME_BEFORE_DELETE
from .models import Character


class CharacterSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.CharField(
        max_length=20,
        error_messages={
            "max_length": "Name must be less than or equal to 20 characters.",
        },
    )
    deletion_time = serializers.SerializerMethodField()

    class Meta:
        model = Character
        fields = ["name", "level", "deletion_time"]
        read_only_fields = ("level",)

    def get_deletion_time(self, instance):
        if instance.deletion_scheduled:
            return instance.deletion_scheduled + timedelta(minutes=TIME_BEFORE_DELETE)
        else:
            return None

    def validate_name(self, value):
        if Character.objects.filter(name=value).exists():
            raise serializers.ValidationError(
                "Character with this name already exists."
            )
        if not re.match("^[a-zA-Z]+$", value):
            raise serializers.ValidationError(
                "Name can only contain a-z and A-Z characters."
            )
        return value

    def create(self, validated_data):
        # Here we are setting the user to the request user
        validated_data["user"] = self.context["request"].user
        return super().create(validated_data)
