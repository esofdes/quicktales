from datetime import timedelta

from celery import shared_task
from django.utils import timezone

from .const import TIME_BEFORE_DELETE
from .models import Character


@shared_task
def delete_character_after_delay(character_id):
    character = Character.objects.get(id=character_id)
    if (
        character.deletion_scheduled
        and timezone.now() - character.deletion_scheduled >= timedelta(minutes=TIME_BEFORE_DELETE)
    ):
        character.delete()
