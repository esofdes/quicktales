from celery_app import app
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from characters.models import Character
from characters.serializers import CharacterSerializer


class CharactersAPITestCase(APITestCase):
    def setUp(self):
        self.user_1 = User.objects.create(username="test_user1")
        self.user_2 = User.objects.create(username="test_user2")
        self.character_1 = Character.objects.create(user=self.user_1, name="CharOne")
        self.character_2 = Character.objects.create(user=self.user_2, name="CharTwo")
        self.my_character_url = reverse("api:my-character")

    def test_get_characters(self):
        url = reverse("api:character-list")
        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        characters = Character.objects.all()
        serializer_data = CharacterSerializer(characters, many=True).data
        self.assertEqual(serializer_data, response.data)

    def test_get_my_character(self):
        self.client.force_login(self.user_1)
        response = self.client.get(self.my_character_url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        character = Character.objects.get(user=self.user_1)
        serializer_data = CharacterSerializer(character).data
        self.assertEqual(serializer_data, response.data)

    def test_get_my_character_not_exist(self):
        user = User.objects.create(username="user")
        self.client.force_login(user)
        response = self.client.get(self.my_character_url)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
        self.assertEqual(Character.objects.filter(user=user).count(), 0)

    def test_character_creation_with_various_names(self):
        user = User.objects.create(username="user")
        self.client.force_login(user)
        valid_names = ["validName", "AValidName", "VaLiDnAmE"]
        for name in valid_names:
            data = {"name": name}
            response = self.client.post(self.my_character_url, data)
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            character = Character.objects.get(name=name)
            self.assertEqual(character.level, 1)
            self.assertEqual(character.name, data["name"])
            character.delete()

        invalid_names = ["invalidName1", "Invalid_Name", "Invalid Name"]
        expected_response_data = {
            "name": ["Name can only contain a-z and A-Z characters."]
        }
        for name in invalid_names:
            data = {"name": name}
            response = self.client.post(self.my_character_url, data)
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(response.data, expected_response_data, response.data)
            self.assertEqual(Character.objects.filter(name=name).count(), 0)

        expected_response_data = {
            "name": ["Name must be less than or equal to 20 characters."]
        }
        response = self.client.post(
            self.my_character_url, {"name": "MoreThanTwentyCharsSs"}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, expected_response_data, response.data)
        self.assertEqual(Character.objects.filter(name=name).count(), 0)

        expected_response_data = {"name": ["Character with this name already exists."]}
        response = self.client.post(
            self.my_character_url, {"name": self.character_1.name}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, expected_response_data, response.data)
        self.assertEqual(Character.objects.filter(name=name).count(), 0)

    def test_create_character_unauthorized(self):
        user = User.objects.create(username="user")
        data = {"name": "char"}
        response = self.client.post(self.my_character_url, data)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)
        self.assertEqual(Character.objects.count(), 2)
        self.assertEqual(Character.objects.filter(user=user).count(), 0)

    def test_create_character_when_exist(self):
        user = User.objects.create(username="user")
        character = Character.objects.create(name="test", user=user)
        character.level = 5
        character.save()
        data = {"name": "char"}
        self.client.force_login(user)
        response = self.client.post(self.my_character_url, data)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertEqual(Character.objects.count(), 3)
        character.refresh_from_db()
        self.assertEqual(character.level, 5)
        self.assertEqual(character.name, "test")

    def test_delete_character_and_undo(self):
        url = reverse("api:my-character")
        self.client.force_login(self.user_1)
        response = self.client.delete(url)
        self.assertEqual(status.HTTP_202_ACCEPTED, response.status_code)
        self.assertEqual(Character.objects.all().count(), 2)
        character = Character.objects.get(user=self.user_1)
        self.assertNotEqual(character.deletion_scheduled, None)
        self.assertNotEqual(character.deletion_task_id, None)
        task_id = character.deletion_task_id
        i = app.control.inspect()
        scheduled_tasks = i.scheduled()
        tusk_result = None
        for worker, tasks in scheduled_tasks.items():
            for task in tasks:
                if task["request"]["id"] == task_id:
                    tusk_result = task
                    break
        self.assertNotEqual(tusk_result, None)
        response = self.client.delete(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(Character.objects.all().count(), 2)
        character.refresh_from_db()
        self.assertEqual(character.deletion_scheduled, None)
        self.assertEqual(character.deletion_task_id, None)
