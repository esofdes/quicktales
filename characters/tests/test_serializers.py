from datetime import timedelta

from django.contrib.auth.models import User

from characters.const import TIME_BEFORE_DELETE
from characters.models import Character
from django.test import TestCase
from characters.serializers import CharacterSerializer


class CharacterTestCase(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create(username="test_user1")
        self.user_2 = User.objects.create(username="test_user2")
        self.character_1 = Character.objects.create(user=self.user_1, name="char1")
        self.character_2 = Character.objects.create(user=self.user_2, name="char2")

    def test_ok(self):
        self.character_1.schedule_delete()
        self.character_1.refresh_from_db()
        deletion_time = self.character_1.deletion_scheduled + timedelta(
            minutes=TIME_BEFORE_DELETE
        )
        expected_data = [
            {"name": "char1", "level": 1, "deletion_time": deletion_time},
            {"name": "char2", "level": 1, "deletion_time": None},
        ]
        characters = Character.objects.all()
        data = CharacterSerializer(characters, many=True).data
        self.assertEqual(expected_data, data, data)
        self.assertEqual(str(self.character_1), self.character_1.name)
        self.character_1.unschedule_delete()
