from django.urls import include, path
from rest_framework import routers
from characters.views import CharacterViewSet, MyCharacterView

app_name = "characters"

router = routers.DefaultRouter()
router.register("character", CharacterViewSet)

urlpatterns = [
    path("me/", MyCharacterView.as_view(), name="my-character"),
    path("", include(router.urls)),
]
