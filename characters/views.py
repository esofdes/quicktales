from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import (
    RetrieveModelMixin,
    ListModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    DestroyModelMixin,
)
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from .models import Character
from .serializers import CharacterSerializer


class CharacterViewSet(GenericViewSet, ListModelMixin):
    queryset = Character.objects.all()
    serializer_class = CharacterSerializer


class MyCharacterView(
    CreateModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
    DestroyModelMixin,
    GenericAPIView,
):
    serializer_class = CharacterSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return Character.objects.get(user=self.request.user)

    def get(self, request, *args, **kwargs):
        try:
            return self.retrieve(request, *args, **kwargs)
        except Character.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        if Character.objects.filter(user=request.user).exists():
            return Response(
                {"detail": "Character already exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        return self.create(request, user=request.user, *args, **kwargs)

    # def put(self, request, *args, **kwargs):
    #     return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.deletion_scheduled:
            instance.unschedule_delete()
            return Response(status=status.HTTP_200_OK)
        else:
            instance.schedule_delete()
            return Response(status=status.HTTP_202_ACCEPTED)
